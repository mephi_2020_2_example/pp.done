package ru.aleksey2093.mephi.pp.done.controller;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import ru.aleksey2093.mephi.pp.done.config.VertxConf;
import ru.aleksey2093.mephi.pp.done.dto.StudentDto;
import ru.aleksey2093.mephi.pp.done.entity.UserEntity;
import ru.aleksey2093.mephi.pp.done.services.RatingService;

@Route(value = "student-rating")
@PageTitle("Оценивание проекта")
@Push
@PreserveOnRefresh
@Slf4j
public class StudentRatingController extends AppLayout {
    VerticalLayout verticalLayout = new VerticalLayout();

    ComboBox<StudentDto> studentDtoComboBox = new ComboBox<>();
    private final TextField studentName = new TextField("Студент");
    private final TextField projectName = new TextField("Проект");
    private final RadioButtonGroup<String> radioButtonGroup = new RadioButtonGroup<>();
    private final TextField comment = new TextField("Комментарий");
    private final Button doneButton = new Button("Сделать выбор");

    RatingService ratingService;

    public StudentRatingController(RatingService ratingService) {
        this.ratingService = ratingService;
        if (VaadinSession.getCurrent().getAttribute(UserEntity.class) == null) {
            UI.getCurrent().navigate("login");
        } else {
            verticalLayout.add("Hi, " + VaadinSession.getCurrent().getAttribute(UserEntity.class).getName());
        }
        radioButtonGroup.setLabel("Допустить к зачету?");
        radioButtonGroup.setItems("Да","Нет");
        radioButtonGroup.setValue("Да");
        studentName.setReadOnly(true);
        projectName.setReadOnly(true);
        setContent(verticalLayout);
        verticalLayout.setHeightFull();
        verticalLayout.setWidthFull();
        verticalLayout.setAlignItems(FlexComponent.Alignment.CENTER);

        doneButton.addClickListener(event -> {
            boolean win = radioButtonGroup.getValue().equalsIgnoreCase("Да");
            UserEntity userEntity = VaadinSession.getCurrent().getAttribute(UserEntity.class);
            JsonObject jsonObject;
            if (ratingService.addRating(userEntity.getId(), studentDtoComboBox.getValue().getId(), win, null, comment.getValue())) {
                jsonObject = new JsonObject()
                        .put("student_id",studentDtoComboBox.getValue().getId())
                        .put("user",userEntity)
                        .put("win",win)
                        .put("comment",comment.getValue())
                        .put("first",true);
            } else {
                jsonObject = new JsonObject()
                        .put("student_id",studentDtoComboBox.getValue().getId())
                        .put("user",userEntity)
                        .put("win",win)
                        .put("comment",comment.getValue())
                        .put("first",false);
            }
            VertxConf.vertx.eventBus().publish("stats",jsonObject);
            studentDtoComboBox.clear();
        });

        studentDtoComboBox.setLabel("Студенты");
        studentDtoComboBox.setWidthFull();

        studentDtoComboBox.setPlaceholder("Выберите студента");
        studentDtoComboBox.addValueChangeListener(event -> {
            if (event.getHasValue().isEmpty()) {
                studentName.setValue("Не выбран");
                projectName.setValue("");
            } else {
                studentName.setValue(event.getValue().getStudentName());
                projectName.setValue(event.getValue().getProjectNumber() + ": " + event.getValue().getProjectName());
            }
            radioButtonGroup.clear();
            comment.clear();
        });
        studentDtoComboBox.setItemLabelGenerator(StudentDto::toComboBoxV1);
        studentDtoComboBox.setItems(ratingService.studentDtoList());

        doneButton.setEnabled(!studentDtoComboBox.isEmpty());

        radioButtonGroup.addValueChangeListener(event -> doneButton.setEnabled(!studentDtoComboBox.isEmpty() && !radioButtonGroup.isEmpty()));

        verticalLayout.add(studentDtoComboBox, studentName, projectName, radioButtonGroup, comment, doneButton);
    }
}
