package ru.aleksey2093.mephi.pp.done.enums.converter;

import ru.aleksey2093.mephi.pp.done.enums.UserRoleEnum;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Optional;

@Converter(autoApply = true)
public class UserRoleEnumConverter implements AttributeConverter<UserRoleEnum,String> {

    @Override
    public String convertToDatabaseColumn(UserRoleEnum userRoleEnum) {
        return Optional.ofNullable(userRoleEnum).map(Object::toString).orElse(null);
    }

    @Override
    public UserRoleEnum convertToEntityAttribute(String s) {
        return Optional.ofNullable(s).map(UserRoleEnum::valueOf).orElse(null);
    }
}
