package ru.aleksey2093.mephi.pp.done.controller;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import lombok.extern.slf4j.Slf4j;
import ru.aleksey2093.mephi.pp.done.entity.UserEntity;

@Route(value = "logout")
@PageTitle("Выход")
@Push
@PreserveOnRefresh
@Slf4j
public class LogoutController extends AppLayout {
    public LogoutController() {
        VaadinSession.getCurrent().setAttribute(UserEntity.class, null);
        UI.getCurrent().navigate("/");
    }
}
