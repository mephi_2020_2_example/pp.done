package ru.aleksey2093.mephi.pp.done.config;

import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DB {
    private static final Logger logger = LoggerFactory.getLogger(DB.class);

    @Bean
    DataSource dataSource(@Value("${db.driver}") String driver, @Value("${db.url}") String url, @Value("${db.username}") String username, @Value("${db.password}") String password) {
        logger.info("init DataSource start");
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setDriverClassName(driver);
        hikariDataSource.setJdbcUrl(url);
        hikariDataSource.setUsername(username);
        hikariDataSource.setPassword(password);
        hikariDataSource.setMaximumPoolSize(10);
        hikariDataSource.setAutoCommit(false);
        hikariDataSource.setConnectionTestQuery("select 1");
        logger.info("init DataSource done");
        return hikariDataSource;
    }
}
