package ru.aleksey2093.mephi.pp.done.controller;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.Scroller;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.shared.Registration;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.MessageConsumer;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.aleksey2093.mephi.pp.done.config.VertxConf;
import ru.aleksey2093.mephi.pp.done.dto.RatingHistoryLog;
import ru.aleksey2093.mephi.pp.done.dto.StudentDto;
import ru.aleksey2093.mephi.pp.done.entity.UserEntity;
import ru.aleksey2093.mephi.pp.done.services.RatingService;
import ru.aleksey2093.mephi.pp.done.services.UserService;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Route(value = "overlay")
@PageTitle("Информация по студенту")
@Push
@PreserveOnRefresh
@Slf4j
public class StudentStatsController extends AppLayout  implements HasUrlParameter<String> {
    VerticalLayout verticalLayout = new VerticalLayout();

    private final TextField studentName = new TextField("Студент");
    private final TextField projectName = new TextField("Проект");
    private final TextField rating = new TextField("Допущен к зачету?");
    private final TextField ratingCount = new TextField("Кол-во голосов");

    private int studentId = -1;

    private final VerticalLayout logRating = new VerticalLayout();

    @Autowired
    RatingService ratingService;

//    public final static Map<StudentStatsController,Boolean> map = new ConcurrentHashMap<>();

    public StudentStatsController() {
        studentName.setWidthFull();
        projectName.setWidthFull();
        rating.setWidthFull();
        ratingCount.setWidthFull();
        studentName.setReadOnly(true);
        projectName.setReadOnly(true);
        rating.setReadOnly(true);
        ratingCount.setReadOnly(true);
        setContent(verticalLayout);
        verticalLayout.setHeightFull();
        verticalLayout.setWidthFull();
        verticalLayout.setAlignItems(FlexComponent.Alignment.CENTER);

        Scroller scroller = new Scroller();
        scroller.setScrollDirection(Scroller.ScrollDirection.VERTICAL);
        scroller.setContent(logRating);
        scroller.setWidthFull();
        scroller.setHeightFull();

        verticalLayout.add(studentName, projectName, rating, ratingCount,scroller);
//        verticalLayout.setPadding(false);
//        verticalLayout.setSpacing(false);

        logRating.setWidthFull();
        logRating.setHeightFull();
        logRating.setAlignItems(FlexComponent.Alignment.START);
//        logRating.setPadding(false);
//        logRating.setPadding(false);
    }

    @Autowired
    UserService userService;

    private MessageConsumer<JsonObject> consumer;

    @Override
    public void setParameter(BeforeEvent beforeEvent, @OptionalParameter String s) {
        String login = beforeEvent.getLocation().getQueryParameters().getParameters().get("login").get(0);
        String password = beforeEvent.getLocation().getQueryParameters().getParameters().get("password").get(0);
        UserEntity userEntity = userService.findUserByLogin(login);
        if (userEntity != null && password.trim().equals(userEntity.getPassword())) {
            VaadinSession.getCurrent().setAttribute(UserEntity.class, userEntity);
//            map.put(this, true);
            if (consumer == null) {
                consumer = VertxConf.vertx.eventBus().consumer("stats", event -> {
                    this.getUI().get().access(()-> {
                        JsonObject jsonObject = event.body();
                        synchronized (this) {
                            int userId = jsonObject.getInteger("userId",-1);
                            UserEntity userEntity2 = this.getUI().get().getSession().getAttribute(UserEntity.class);
                            if (userEntity2 != null && userEntity2.getId() == userId) {
                                if (jsonObject.containsKey("set_student_id")) {
                                    setStudentId(jsonObject.getInteger("set_student_id",-1));
                                }
                                if (jsonObject.getBoolean("refreshInfo",false)) {
                                    refreshInfo(jsonObject.getBoolean("log",false));
                                }
                            }
                            if (studentId == -1) {
                                return;
                            }
                            if (jsonObject.getInteger("student_id",-1) == studentId) {
                                String user_login = jsonObject.getString("user_login");
                                boolean win = jsonObject.getBoolean("win");
                                String comment = jsonObject.getString("comment");
                                boolean first = jsonObject.getBoolean("first");
                                addLog(user_login, win, comment, first);
                            }
                        }
                    });
                });
            }
        }
    }

    @Override
    public Registration addDetachListener(ComponentEventListener<DetachEvent> listener) {
        Registration registration = super.addDetachListener(listener);
//        map.remove(this);
        if (consumer != null) {
            try {
                consumer.unregister();
            } catch (Exception e) {
                log.error(null, e);
            }
        }
        return registration;
    }

    private void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    private void refreshInfo(boolean log) {
        this.getUI().get().access(()-> {
            StudentDto studentDto = ratingService.getStudentDtoById(studentId);
            studentName.setValue(studentDto.getStudentName());
            projectName.setValue(studentDto.getProjectNumber() + ": " + studentDto.getProjectName());
            rating.setValue(studentDto.getRatingAsPercent());
            ratingCount.setValue(String.valueOf(studentDto.getRatingCount()));
            if (log) {
                synchronized (logRating) {
                    logRating.removeAll();
                }
                synchronized (logRating) {
                    for (RatingHistoryLog historyLog : ratingService.getRatingHistory(studentId)) {
                        logRating.add(historyLog.asV1());
                    }
                }
            }
        });
    }

    private void addLog(String user_login, boolean win, String comment, boolean first) {
        UserEntity userEntity = userService.findUserByLogin(user_login);
        RatingHistoryLog historyLog = RatingHistoryLog.builder()
                .ts(LocalDateTime.now())
                .name(userEntity.getName())
                .userRoleEnum(userEntity.getRole())
                .message(comment)
                .first(first)
                .win(win)
                .build();

        synchronized (logRating) {
            logRating.addComponentAsFirst(historyLog.asV1());
        }
        refreshInfo(false);
    }
}
