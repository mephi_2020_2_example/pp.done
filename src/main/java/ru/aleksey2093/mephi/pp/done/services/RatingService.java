package ru.aleksey2093.mephi.pp.done.services;

import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import ru.aleksey2093.mephi.pp.done.config.InitDB;
import ru.aleksey2093.mephi.pp.done.dto.RatingHistoryLog;
import ru.aleksey2093.mephi.pp.done.dto.StudentDto;
import ru.aleksey2093.mephi.pp.done.dto.mapper.StudentDtoMapper;
import ru.aleksey2093.mephi.pp.done.entity.RatingHistoryEntity;
import ru.aleksey2093.mephi.pp.done.entity.StudentEntity;
import ru.aleksey2093.mephi.pp.done.entity.UserEntity;
import ru.aleksey2093.mephi.pp.done.enums.UserRoleEnum;
import ru.aleksey2093.mephi.pp.done.repository.RatingHistoryRepository;
import ru.aleksey2093.mephi.pp.done.repository.StudentRepository;
import ru.aleksey2093.mephi.pp.done.repository.UserRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@Slf4j
public class RatingService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RatingHistoryRepository ratingHistoryRepository;

    @Autowired
    StudentRepository studentRepository;

//    @Transactional
//    public void addMessage(int user_id, int student_id, String message) {
//        UserEntity userEntity = userRepository.findById(user_id).get();
//        StudentEntity studentEntity = studentRepository.findById(student_id).get();
//    }

    @Transactional
    public boolean addRating(int user_id, int student_id, boolean win, Boolean winTwitch, String comment) {
        UserEntity userEntity = userRepository.findById(user_id).get();
        StudentEntity studentEntity = studentRepository.findById(student_id).get();

        var r1 = ratingHistoryRepository.findByUserAndStudent(userEntity, studentEntity);
        if (r1.isPresent()) {
            if (userEntity.getRole() == UserRoleEnum.ARBITRAGE) {
                r1.get().setTs(LocalDateTime.now());
                r1.get().setWin(win);
                ratingHistoryRepository.save(r1.get());
            } else {
                return false;
            }
        } else {
            RatingHistoryEntity ratingHistoryEntity = RatingHistoryEntity.builder()
                    .student(studentEntity)
                    .user(userEntity)
                    .message(comment)
                    .ts(LocalDateTime.now())
                    .win(win)
                    .build();
            ratingHistoryRepository.save(ratingHistoryEntity);
        }

        if (winTwitch != null && userEntity.getRole() == UserRoleEnum.ARBITRAGE) {
            userEntity = userRepository.findById(InitDB.simpleUserId).get();
            ratingHistoryRepository.findByUserAndStudent(userEntity, studentEntity);
            if (r1.isPresent()) {
                r1.get().setTs(LocalDateTime.now());
                r1.get().setWin(win);
                ratingHistoryRepository.save(r1.get());
            } else {
                RatingHistoryEntity ratingHistoryEntity = RatingHistoryEntity.builder()
                        .student(studentEntity)
                        .user(userEntity)
                        .ts(LocalDateTime.now())
                        .win(win)
                        .build();
                ratingHistoryRepository.save(ratingHistoryEntity);
            }
        }

        int countTrue = ratingHistoryRepository.countByStudentAndWin(studentEntity,true);
        int countFalse = ratingHistoryRepository.countByStudentAndWin(studentEntity,false);
        int count = countTrue + countFalse;

        double avg = (double) countTrue / count;

        studentEntity.setRating(avg);
        studentEntity.setRatingCount(count);

        studentRepository.save(studentEntity);

        return true;
    }

    @Transactional
    public StudentDto getStudentDtoById(int student_id) {
        var student = studentRepository.findById(student_id)
                .orElseGet(() -> StudentEntity.builder().id(null).studentName("Нет меня").build());
        return StudentDtoMapper.MAPPER.toDto(student);
    }

    @Transactional
    public List<StudentDto> studentDtoList() {
        List<StudentDto> list = new ArrayList<>();
        for (StudentEntity studentEntity : studentRepository.findAll()) {
            list.add(StudentDtoMapper.MAPPER.toDto(studentEntity));
        }
        list.sort(Comparator.comparingInt(StudentDto::getRatingCount));
        return list;
    }

    @Transactional
    public List<RatingHistoryLog> getRatingHistory(int studentId) {
        List<RatingHistoryLog> list = new ArrayList<>();
        for (var item : ratingHistoryRepository.findAllLogByStudentId(studentId)) {
            list.add(item);
        }
        list.sort((x1,x2)-> x2.getTs().compareTo(x1.getTs()));
        return list;
    }
}
