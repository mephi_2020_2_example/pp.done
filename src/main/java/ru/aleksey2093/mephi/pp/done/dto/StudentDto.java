package ru.aleksey2093.mephi.pp.done.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class StudentDto {
    private Integer id;

    private String studentName;

    private int projectNumber;

    private String projectName;

    private double rating;

    private int ratingCount;

    public String toComboBoxV1() {
        return studentName + ", " + projectNumber + ": " + projectName;
    }

    public String getRatingAsPercent() {
        return rating * 100 + "%";
    }
}
