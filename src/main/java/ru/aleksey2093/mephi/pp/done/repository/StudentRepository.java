package ru.aleksey2093.mephi.pp.done.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.aleksey2093.mephi.pp.done.entity.StudentEntity;
import ru.aleksey2093.mephi.pp.done.entity.UserEntity;

@Repository
public interface StudentRepository extends CrudRepository<StudentEntity,Integer> {

}
