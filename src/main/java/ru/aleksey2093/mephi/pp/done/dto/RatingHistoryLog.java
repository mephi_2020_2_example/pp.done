package ru.aleksey2093.mephi.pp.done.dto;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Span;
import lombok.*;
import org.codehaus.plexus.util.StringUtils;
import ru.aleksey2093.mephi.pp.done.enums.UserRoleEnum;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Builder
public class RatingHistoryLog {
    private LocalDateTime ts;
    private String name;
    private UserRoleEnum userRoleEnum;
    private boolean win;
    private boolean first;
    private String message;

    public Span asV1() {
        String text = ts.atOffset(ZoneOffset.ofHours(3)).format(DateTimeFormatter.ofPattern("hh:mm")) + " " + name;
        if (win) {
            if (first) {
                text += " допускает к зачету";
            } else {
                text += " пытается допустить к зачету";
            }
        } else {
            if (first) {
                text += " не допускает к зачету";
            } else {
                text += " пытается не допустить к зачету";
            }
        }
        if (StringUtils.isNotBlank(message)) {
            text += " и говорит: " + '"' + message.trim() + '"';
        }
        Span span = new Span(text);
        if (win) {
            span.getStyle().set("color","green");
        } else {
            span.getStyle().set("color","red");
        }
        if (userRoleEnum == UserRoleEnum.ARBITRAGE) {
            span.getStyle().set("font-weight","bold");
        }
        return span;
    }
}
