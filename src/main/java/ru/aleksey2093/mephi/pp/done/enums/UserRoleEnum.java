package ru.aleksey2093.mephi.pp.done.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public enum  UserRoleEnum {
    ARBITRAGE,
    PP_BOSS,
    TWITCH_PREMIUM,
    TWITCH_SIMPLE_USER
}
