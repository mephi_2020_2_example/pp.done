package ru.aleksey2093.mephi.pp.done.controller;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import ru.aleksey2093.mephi.pp.done.entity.UserEntity;
import ru.aleksey2093.mephi.pp.done.enums.UserRoleEnum;
import ru.aleksey2093.mephi.pp.done.services.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Route(value = "regist")
@PageTitle("Регистрация пользователей")
@Push
@Slf4j
public class RegistrationNewUserController extends AppLayout {

    VerticalLayout verticalLayout = new VerticalLayout();

    @Autowired
    UserService userService;

    private TextField login = new TextField("Логин");
    private TextField password = new TextField("Пароль");
    private TextField name = new TextField("ФИО");
    private ComboBox<UserRoleEnum> userRoleEnumComboBox = new ComboBox<>("Роль");
    private Button buttonReg = new Button("Регистрация");

    public RegistrationNewUserController() {
        UserEntity userEntity = VaadinSession.getCurrent().getAttribute(UserEntity.class);
        if (userEntity == null) {
            UI.getCurrent().navigate("login");
            return;
        }
        if (userEntity.getRole() == UserRoleEnum.ARBITRAGE) {
            userRoleEnumComboBox.setItems(UserRoleEnum.values());
        } else if (userEntity.getRole() == UserRoleEnum.PP_BOSS) {
            List<UserRoleEnum> list = new ArrayList<>(Arrays.asList(UserRoleEnum.values()));
            list.remove(UserRoleEnum.ARBITRAGE);
            userRoleEnumComboBox.setItems(list);
        } else {
            Dialog dialog = new Dialog();
            dialog.setModal(true);
            dialog.setCloseOnOutsideClick(true);
            dialog.setCloseOnEsc(true);
            dialog.add("Вашего уровня доступа не достаточно для регистрации новых пользователей");
            dialog.open();
            return;
        }

        buttonReg.addClickListener(new ComponentEventListener<ClickEvent<Button>>() {
            @Override
            public void onComponentEvent(ClickEvent<Button> event) {
                if (login.getValue().trim().isEmpty()) {
                    Dialog dialog = new Dialog();
                    dialog.setModal(true);
                    dialog.setCloseOnOutsideClick(true);
                    dialog.setCloseOnEsc(true);
                    dialog.add("Введите логин");
                    dialog.open();
                } else if (password.getValue().trim().isEmpty()) {
                    Dialog dialog = new Dialog();
                    dialog.setModal(true);
                    dialog.setCloseOnOutsideClick(true);
                    dialog.setCloseOnEsc(true);
                    dialog.add("Введите пароль");
                    dialog.open();
                } else if (password.getValue().trim().length() < 3) {
                    Dialog dialog = new Dialog();
                    dialog.setModal(true);
                    dialog.setCloseOnOutsideClick(true);
                    dialog.setCloseOnEsc(true);
                    dialog.add("Пароль должен быть от трех символов");
                    dialog.open();
                } else if (name.getValue().trim().isEmpty()) {
                    Dialog dialog = new Dialog();
                    dialog.setModal(true);
                    dialog.setCloseOnOutsideClick(true);
                    dialog.setCloseOnEsc(true);
                    dialog.add("Введите имя");
                    dialog.open();
                } else if (userRoleEnumComboBox.isEmpty()) {
                    Dialog dialog = new Dialog();
                    dialog.setModal(true);
                    dialog.setCloseOnOutsideClick(true);
                    dialog.setCloseOnEsc(true);
                    dialog.add("Выберите роль");
                    dialog.open();
                } else {
                    UserEntity userEntity2 = userService.findUserByLogin(login.getValue());
                    if (userEntity2 == null) {
                        userService.reg(login.getValue(),password.getValue(),name.getValue(),userRoleEnumComboBox.getValue());
                        Dialog dialog = new Dialog();
                        dialog.setModal(true);
                        dialog.setCloseOnOutsideClick(true);
                        dialog.setCloseOnEsc(true);
                        dialog.add("Пользователь создан");
                        dialog.addDialogCloseActionListener(new ComponentEventListener<Dialog.DialogCloseActionEvent>() {
                            @Override
                            public void onComponentEvent(Dialog.DialogCloseActionEvent event) {
                                login.clear();
                                password.clear();
                                name.clear();
                                userRoleEnumComboBox.clear();
                            }
                        });
                        dialog.open();
                    } else if (userEntity.getRole() == UserRoleEnum.ARBITRAGE) {
                        Dialog dialog = new Dialog();
                        dialog.setModal(true);
                        dialog.setCloseOnOutsideClick(true);
                        dialog.setCloseOnEsc(true);
                        dialog.add("Хотите изменить данные пользователя " + login.getValue().trim());

                        Button button = new Button("Изменить");
                        button.addClickListener(new ComponentEventListener<ClickEvent<Button>>() {
                            @Override
                            public void onComponentEvent(ClickEvent<Button> event) {
                                userService.change(userEntity2.getId(), login.getValue(), password.getValue(), name.getValue(), userRoleEnumComboBox.getValue());
                                Dialog dialog = new Dialog();
                                dialog.setModal(true);
                                dialog.setCloseOnOutsideClick(true);
                                dialog.setCloseOnEsc(true);
                                dialog.add("Пользователь " + login.getValue().trim() + " изменен");
                                dialog.addDialogCloseActionListener(new ComponentEventListener<Dialog.DialogCloseActionEvent>() {
                                    @Override
                                    public void onComponentEvent(Dialog.DialogCloseActionEvent event) {
                                        login.clear();
                                        password.clear();
                                        name.clear();
                                        userRoleEnumComboBox.clear();
                                    }
                                });
                                dialog.open();
                            }
                        });
                        dialog.add(button);

                        dialog.open();
                    } else {
                        Dialog dialog = new Dialog();
                        dialog.setModal(true);
                        dialog.setCloseOnOutsideClick(true);
                        dialog.setCloseOnEsc(true);
                        dialog.add("Недостаточно прав для редактирования данных пользователя");
                        dialog.open();
                    }
                }
            }
        });

        login.setWidthFull();
        password.setWidthFull();
        name.setWidthFull();
        userRoleEnumComboBox.setWidthFull();

        verticalLayout.add(login,password,name,userRoleEnumComboBox,buttonReg);

        setContent(verticalLayout);
        verticalLayout.setWidthFull();
        verticalLayout.setHeightFull();
    }
}
