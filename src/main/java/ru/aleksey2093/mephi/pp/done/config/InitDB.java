package ru.aleksey2093.mephi.pp.done.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.aleksey2093.mephi.pp.done.entity.UserEntity;
import ru.aleksey2093.mephi.pp.done.enums.UserRoleEnum;
import ru.aleksey2093.mephi.pp.done.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.UUID;

@Service
public class InitDB implements CommandLineRunner {

    public static int simpleUserId;

    @Autowired
    UserRepository userRepository;

    @Transactional
    public void test1() {
        UserEntity userEntity = new UserEntity();
        userEntity.setName("aleksey frolov");
        userEntity.setLogin("alex");
        userEntity.setPassword(UUID.randomUUID().toString().substring(0,8));
        userEntity.setRole(UserRoleEnum.ARBITRAGE);
        if (userRepository.findByLogin(userEntity.getLogin()) == null) {
            userRepository.save(userEntity);
        }

        userEntity = new UserEntity();
        userEntity.setName("twitch");
        userEntity.setLogin("twitch");
        userEntity.setPassword(UUID.randomUUID().toString().substring(0,8));
        userEntity.setRole(UserRoleEnum.TWITCH_SIMPLE_USER);
        if (userRepository.findByLogin(userEntity.getLogin()) == null) {
            userRepository.save(userEntity);
        }
        simpleUserId = userRepository.findByLogin("twitch").getId();

        for (UserEntity entity : userRepository.findAll()) {
            if (entity.getPassword() == null || entity.getPassword().trim().isEmpty() || entity.getPassword().equals("0")) {
                entity.setPassword(UUID.randomUUID().toString().substring(0,8));
                userRepository.save(entity);
            }
        }
    }

    @Override
    public void run(String... args) throws Exception {
        test1();
    }
}
