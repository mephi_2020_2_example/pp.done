package ru.aleksey2093.mephi.pp.done.entity;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "students",schema = "ppdone")
public class StudentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(nullable = false)
    private String studentName;

    @Column(nullable = false)
    private int projectNumber;

    @Column(nullable = false)
    private String projectName;

    @Column(nullable = false)
    private double rating;

    @Column(nullable = false)
    private int ratingCount;
}
