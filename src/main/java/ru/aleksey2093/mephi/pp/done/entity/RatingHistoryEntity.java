package ru.aleksey2093.mephi.pp.done.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "rating_histories",schema = "ppdone",
indexes = {@Index(columnList = "student_id,user_id",unique = true)})
public class RatingHistoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @JoinColumn(nullable = false)
    LocalDateTime ts;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id",nullable = false)
    StudentEntity student;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",nullable = false)
    UserEntity user;

    @Column(nullable = false)
    boolean win;

    private String message;
}
