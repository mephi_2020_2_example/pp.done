package ru.aleksey2093.mephi.pp.done.controller;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.VaadinSession;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.aleksey2093.mephi.pp.done.config.VertxConf;
import ru.aleksey2093.mephi.pp.done.dto.StudentDto;
import ru.aleksey2093.mephi.pp.done.entity.UserEntity;
import ru.aleksey2093.mephi.pp.done.enums.UserRoleEnum;
import ru.aleksey2093.mephi.pp.done.services.RatingService;
import ru.aleksey2093.mephi.pp.done.services.UserService;

import java.util.ArrayList;
import java.util.Collections;

@Route(value = "obs")
@PageTitle("Док панель")
@Push
@Slf4j
public class ObsPanelController extends AppLayout implements HasUrlParameter<String> {

    @Autowired
    UserService userService;

    @Autowired
    RatingService ratingService;

    VerticalLayout layout = new VerticalLayout();

    TextField userName = new TextField("Вы");
    Button buttonRefresh = new Button(new Icon(VaadinIcon.REFRESH));

    ComboBox<StudentDto> studentDtoComboBox = new ComboBox<>();
    private final TextField studentName = new TextField("Студент");

    private final RadioButtonGroup<String> radioButtonGroupBoss = new RadioButtonGroup<>();
    private final RadioButtonGroup<String> radioButtonGroupTwitch = new RadioButtonGroup<>();

    private final Button buttonOk = new Button("Зафиксировать");

    public ObsPanelController() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();

        radioButtonGroupBoss.setLabel("Допустить (я)?");
        radioButtonGroupBoss.setItems("Да","Нет");
        radioButtonGroupBoss.setValue("Да");

        radioButtonGroupTwitch.setLabel("Допустить (чат)?");
        radioButtonGroupTwitch.setItems("Да","Нет");
        radioButtonGroupTwitch.setValue("Да");

        {
            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.add(radioButtonGroupBoss);
            horizontalLayout.add(verticalLayout);
            horizontalLayout.setSpacing(false);
            horizontalLayout.setPadding(false);
        }

        {
            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.add(radioButtonGroupTwitch);
            horizontalLayout.add(verticalLayout);
            horizontalLayout.setSpacing(false);
            horizontalLayout.setPadding(false);
        }

        horizontalLayout.setWidthFull();

        studentName.setReadOnly(true);
        setContent(layout);
        layout.setWidthFull();
        layout.setHeightFull();
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        layout.setSpacing(false);
        layout.setPadding(false);

        userName.setReadOnly(true);

        HorizontalLayout horizontalLayout2 = new HorizontalLayout();
        horizontalLayout2.setWidthFull();
        horizontalLayout2.setAlignItems(FlexComponent.Alignment.CENTER);
        horizontalLayout2.add(userName,buttonRefresh);

        layout.add(horizontalLayout2,studentDtoComboBox,studentName,horizontalLayout,buttonOk);

        studentDtoComboBox.setLabel("Студенты");
        studentDtoComboBox.setWidthFull();

        buttonRefresh.addClickListener(event -> {
            radioButtonGroupBoss.clear();
            radioButtonGroupTwitch.clear();
            studentDtoComboBox.setItemLabelGenerator(StudentDto::toComboBoxV1);
        });

        studentDtoComboBox.setPlaceholder("Выберите студента");
        studentDtoComboBox.addValueChangeListener(event -> {
            if (event.getHasValue().isEmpty()) {
                studentName.setValue("Не выбран");
            } else {
                studentName.setValue(event.getValue().getStudentName());
                var user = VaadinSession.getCurrent().getAttribute(UserEntity.class);
//                for (StudentStatsController studentStatsController : StudentStatsController.map.keySet()) {
//                    var user2 = studentStatsController.getSession().getAttribute(UserEntity.class);
//                    if (user2 != null && user.getId().intValue() == user2.getId().intValue()) {
//                        studentStatsController.setStudentId(event.getValue().getId());
//                        studentStatsController.refreshInfo(true);
//                    }
//                }
                JsonObject jsonObject = new JsonObject()
                        .put("userId",user.getId())
                        .put("set_student_id",event.getValue().getId())
                        .put("refreshInfo",true)
                        .put("log",true);
                VertxConf.vertx.eventBus().publish("stats",jsonObject);
            }
            radioButtonGroupBoss.clear();
            radioButtonGroupTwitch.clear();
        });
        studentDtoComboBox.setItemLabelGenerator(StudentDto::toComboBoxV1);

        buttonOk.setEnabled(!studentDtoComboBox.isEmpty());

        radioButtonGroupBoss.addValueChangeListener(event -> buttonOk.setEnabled(!studentDtoComboBox.isEmpty() && !radioButtonGroupBoss.isEmpty() && !radioButtonGroupTwitch.isEmpty()));
        radioButtonGroupTwitch.addValueChangeListener(event -> buttonOk.setEnabled(!studentDtoComboBox.isEmpty() && !radioButtonGroupBoss.isEmpty() && !radioButtonGroupTwitch.isEmpty()));

        buttonOk.addClickListener(event -> {
            ratingService.addRating(VaadinSession.getCurrent().getAttribute(UserEntity.class).getId(),
                    studentDtoComboBox.getValue().getId(),
                    "Да".equalsIgnoreCase(radioButtonGroupBoss.getValue()),
                    "Да".equalsIgnoreCase(radioButtonGroupTwitch.getValue()),null);


            JsonObject jsonObject = new JsonObject()
                    .put("student_id",studentDtoComboBox.getValue().getId())
                    .put("user_login",VaadinSession.getCurrent().getAttribute(UserEntity.class).getLogin())
                    .put("win", "Да".equalsIgnoreCase(radioButtonGroupBoss.getValue()))
                    .put("comment",(String) null)
                    .put("first",true);
            VertxConf.vertx.eventBus().publish("stats",jsonObject);

            jsonObject = new JsonObject()
                    .put("student_id",studentDtoComboBox.getValue().getId())
                    .put("user_login","twitch")
                    .put("win", "Да".equalsIgnoreCase(radioButtonGroupTwitch.getValue()))
                    .put("comment",(String) null)
                    .put("first",true);
            VertxConf.vertx.eventBus().publish("stats",jsonObject);

//            StudentStatsController.addLog(studentDtoComboBox.getValue().getId(),
//                    VaadinSession.getCurrent().getAttribute(UserEntity.class),
//                    "Да".equalsIgnoreCase(radioButtonGroupBoss.getValue()),null,true);
//
//            StudentStatsController.addLog(studentDtoComboBox.getValue().getId(),
//                    userService.findUserByLogin("twitch"),
//                    "Да".equalsIgnoreCase(radioButtonGroupTwitch.getValue()),null,true);

            studentDtoComboBox.clear();
            radioButtonGroupBoss.clear();
            radioButtonGroupTwitch.clear();
        });
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, @OptionalParameter String s) {
        String login = beforeEvent.getLocation().getQueryParameters().getParameters().getOrDefault("login", Collections.singletonList(null)).get(0);
        String password = beforeEvent.getLocation().getQueryParameters().getParameters().getOrDefault("password",Collections.singletonList(null)).get(0);
        UserEntity userEntity = login == null ? null : userService.findUserByLogin(login);
        if (userEntity != null && password.trim().equals(userEntity.getPassword())
                && userEntity.getRole() == UserRoleEnum.ARBITRAGE) {
            VaadinSession.getCurrent().setAttribute(UserEntity.class, userEntity);
            userName.setValue(userEntity.getName());
            buttonRefresh.setEnabled(true);
            buttonRefresh.setVisible(true);
            studentDtoComboBox.setItems(ratingService.studentDtoList());
        } else {
            buttonRefresh.setEnabled(false);
            buttonRefresh.setVisible(false);
            if (userEntity != null && userEntity.getRole() != UserRoleEnum.ARBITRAGE) {
                userName.setValue("Вы не арбитр");
            } else {
                userName.setValue("Ошибка авторизации");
            }
            studentDtoComboBox.setItems(new ArrayList<>());
        }
    }
}
