package ru.aleksey2093.mephi.pp.done.controller;

import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.login.AbstractLogin;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.PreserveOnRefresh;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.aleksey2093.mephi.pp.done.entity.UserEntity;
import ru.aleksey2093.mephi.pp.done.services.UserService;


@Route(value = "login")
@PageTitle("Авторизация")
@Push
@PreserveOnRefresh
@Slf4j
public class LoginController extends AppLayout {
    VerticalLayout layout = new VerticalLayout();

    @Autowired
    UserService userService;

    public LoginController() {
        layout.setWidthFull();
        layout.setHeightFull();
        layout.setAlignItems(FlexComponent.Alignment.CENTER);

        LoginForm loginForm = new LoginForm();
        loginForm.setEnabled(true);
        loginForm.setForgotPasswordButtonVisible(false);
        loginForm.addLoginListener(new ComponentEventListener<AbstractLogin.LoginEvent>() {
            @Override
            public void onComponentEvent(AbstractLogin.LoginEvent loginEvent) {
                login(loginEvent.getUsername(),loginEvent.getPassword());
            }
        });
        loginForm.addForgotPasswordListener(new ComponentEventListener<AbstractLogin.ForgotPasswordEvent>() {
            @Override
            public void onComponentEvent(AbstractLogin.ForgotPasswordEvent forgotPasswordEvent) {
                Dialog dialog = new Dialog();
                dialog.add(new Text("Обратитесь к администратору сайта"));
                dialog.setModal(true);
                dialog.setCloseOnOutsideClick(true);
                dialog.open();
            }
        });

        layout.add(loginForm);

        setContent(layout);
    }

    private void login(String login, String password) {
        UserEntity userEntity = userService.findUserByLogin(login);
        if (userEntity != null && password.trim().equalsIgnoreCase(userEntity.getPassword())) {
            VaadinSession.getCurrent().setAttribute(UserEntity.class,userEntity);
            UI.getCurrent().getPage().getHistory().back();
        } else {
            Dialog dialog = new Dialog();
            dialog.add(new Text("Не удалось найти пользователя в системе"));
            dialog.setModal(true);
            dialog.setCloseOnOutsideClick(true);
            dialog.open();
        }
    }
}