package ru.aleksey2093.mephi.pp.done.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.aleksey2093.mephi.pp.done.dto.StudentDto;
import ru.aleksey2093.mephi.pp.done.entity.StudentEntity;

@Mapper
public interface StudentDtoMapper {
    StudentDtoMapper MAPPER = Mappers.getMapper(StudentDtoMapper.class);

    StudentDto toDto(StudentEntity studentEntity);
}
