package ru.aleksey2093.mephi.pp.done.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.aleksey2093.mephi.pp.done.dto.RatingHistoryLog;
import ru.aleksey2093.mephi.pp.done.entity.RatingHistoryEntity;
import ru.aleksey2093.mephi.pp.done.entity.StudentEntity;
import ru.aleksey2093.mephi.pp.done.entity.UserEntity;

import java.util.Optional;

@Repository
public interface RatingHistoryRepository extends CrudRepository<RatingHistoryEntity,Integer> {
    Optional<RatingHistoryEntity> findByUserAndStudent(UserEntity userEntity, StudentEntity studentEntity);

    int countByStudent(StudentEntity studentEntity);

    @Query(value = "select count(r) from RatingHistoryEntity r where r.student=:student and r.win=:win")
    int countByStudentAndWin(StudentEntity student, boolean win);

    @Query(value = "select r from RatingHistoryEntity r where r.student.id=:studentId")
    Iterable<RatingHistoryEntity> findAllByStudentId(int studentId);

    @Query(value = "select new ru.aleksey2093.mephi.pp.done.dto.RatingHistoryLog(r.ts,r.user.name,r.user.role,r.win,true,r.message) " +
            "from RatingHistoryEntity r where r.student.id=:studentId")
    Iterable<RatingHistoryLog> findAllLogByStudentId(int studentId);
}