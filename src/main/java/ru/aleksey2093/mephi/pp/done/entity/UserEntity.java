package ru.aleksey2093.mephi.pp.done.entity;

import lombok.*;
import ru.aleksey2093.mephi.pp.done.enums.UserRoleEnum;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "users",schema = "ppdone")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(unique = true,nullable = false)
    String login;

    @Column(nullable = false)
    String password;

    @Column(nullable = false)
    String name;

    @Column(nullable = false)
    UserRoleEnum role;
}
