package ru.aleksey2093.mephi.pp.done.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.aleksey2093.mephi.pp.done.entity.UserEntity;
import ru.aleksey2093.mephi.pp.done.enums.UserRoleEnum;
import ru.aleksey2093.mephi.pp.done.repository.UserRepository;

import javax.transaction.Transactional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Transactional
    public UserEntity findUserByLogin(String login) {
        return userRepository.findByLogin(login.toLowerCase().trim());
    }

    @Transactional
    public void reg(String login, String password, String name, UserRoleEnum roleEnum) {
        UserEntity userEntity = UserEntity.builder()
                .login(login.trim().toLowerCase())
                .password(password.trim())
                .name(name.trim())
                .role(roleEnum)
                .build();
        userRepository.save(userEntity);
    }

    @Transactional
    public void change(int id, String login, String password, String name, UserRoleEnum roleEnum) {
        UserEntity userEntity = userRepository.findById(id).get();
        userEntity.setLogin(login.toLowerCase().trim());
        userEntity.setPassword(password.trim());
        userEntity.setName(name.trim());
        userEntity.setRole(roleEnum);
        userRepository.save(userEntity);
    }

    @Transactional
    public UserEntity changePassword(Integer id, String oldPassword, String newPassword) {
        UserEntity userEntity = userRepository.findById(id).orElse(null);
        if (userEntity != null) {
            if (oldPassword.trim().equals(userEntity.getPassword())) {
                userEntity.setPassword(newPassword.trim());
                userRepository.save(userEntity);
                return userEntity;
            }
        }
        return null;
    }
}
